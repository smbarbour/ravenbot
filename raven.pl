#!/usr/bin/perl -w
use strict;
use feature "state";
use Config::IniFiles;
use Data::Dumper;
use Storable;
use DateTime;
use Switch;
use Time::Format;
use WebService::GData::YouTube;
use LWP::Simple;
use Data::Validate::URI qw(is_web_uri);
use POE qw(Component::IRC Component::IRC::Plugin::NickServID Component::IRC::Plugin::BotAddressed Component::IRC::Plugin::Logger Component::IRC::Plugin::Connector Wheel::ReadLine);
#use POE::Component::IRC;
#use POE::Component::IRC::Plugin::Console;
#use POE::Component::IRC::Plugin::NickServID;
#use POE::Component::IRC::Plugin::BotAddressed;
#use POE::Component::IRC::Plugin::Logger;
#use POE::Component::IRC::Plugin::Connector;
use WWW::YouTube::Info;
use Google::Search;
use DBI;
use Net::Twitter;

my $cfg = new Config::IniFiles( -file => 'raven.ini' );

my $nick = $cfg->val('IRC','Nick');
my $user = $cfg->val('IRC','Username');
my $name = $cfg->val('IRC','Ircname');
my $server = $cfg->val('IRC','Server');
my $port = $cfg->val('IRC','Port');
my $NSPass = $cfg->val('IRC','NSPassword');

my $twitter = Net::Twitter->new(
	ssl => 1,
	traits => [qw/API::RESTv1_1 OAuth/],
	consumer_key => $cfg->val('Twitter','ckey'),
	consumer_secret => $cfg->val('Twitter','csecret'),
	access_token => $cfg->val('Twitter','token'),
	access_token_secret => $cfg->val('Twitter','tsecret')
);

my $dbh = DBI->connect("dbi:SQLite:dbname=raven.db","","") or die $DBI::errstr;
my @tables = $dbh->tables();
my @help = (
	['.8ball','Predict the future with yes/no questions. - ex. .8ball Is Raven awesome?'],
	['.tell','Leave a message for a user - ex. .tell user The server is down again.'],
	['.google','Search Google and return the first result'],
	['.youtube','Search YouTube and return the first result'],
	['.wiki','Search for a wiki on the selected topic'],
	['.down','Check if a website is down.'],
	['.quote','Record or play back a quote. ".quote" random quote from any user, ".quote user" random quote for the user, ".quote user Message to be quoted" save a new quote'],
	['.info','Record or play back stored info'],
	['.define','Look up a definition of a word'],
	['.urban','Search the Urban Dictionary'],
	['.devbukkit','Search dev.bukkit.org for plugins'],
	['.seen','Show last activity for a user'],
	['.ann','Search for anime on Anime News Network'],
	['.qlist','List all quotes by a specific user'],
	['.ilist','List all info entries'],
	['.curseforge','Search for Minecraft Mods listed on CurseForge'],
);
my @ophelp = (
	['join','Join a channel'],
	['part','Leave a channel'],
	['op','Add an op'],
	['deop','Remove an op'],
	['speakas','Make the bot say something in a channel or to a user - ex. /msg RavenBot #MyChannel What are you doing, Dave?'],
	['emoteas','Make the bot perform a CTCP ACTION [/me] in a channel or to a user chat'],
	['shutdown','Terminate the bot'],
);

my $URLValidator = Data::Validate::URI->new();
# Create tables (if they don't exist)
$dbh->do("CREATE TABLE IF NOT EXISTS Channels(name)");
$dbh->do("CREATE TABLE IF NOT EXISTS Ops(name)");
$dbh->do("CREATE TABLE IF NOT EXISTS Tells(sender, rcpt, channel, message)");
$dbh->do("CREATE TABLE IF NOT EXISTS Info(key PRIMARY KEY, data)");
$dbh->do("CREATE TABLE IF NOT EXISTS Quotes(user, data)");
$dbh->do("CREATE TABLE IF NOT EXISTS LastSeen(user PRIMARY KEY, timestamp)");

# Prepare Database statements
my $addQuote = $dbh->prepare("INSERT INTO Quotes(user, data) VALUES (?, ?);");
my $getUserQuote = $dbh->prepare("SELECT data FROM Quotes WHERE user = ? ORDER BY RANDOM () LIMIT 1;");
my $getUserQuoteAll = $dbh->prepare("SELECT data FROM Quotes WHERE user = ?;");
my $getAnyQuote = $dbh->prepare("SELECT user, data FROM Quotes ORDER BY RANDOM () LIMIT 1;");
my $getSpecificQuote = $dbh->prepare("SELECT data FROM Quotes WHERE user = ? AND data = ?;");
my $removeQuote = $dbh->prepare("DELETE FROM Quotes WHERE user = ? AND data = ?;");
my $updateLastSeen = $dbh->prepare("REPLACE INTO LastSeen(user, timestamp) VALUES (?, ?);");
my $getLastSeen = $dbh->prepare("SELECT timestamp FROM LastSeen WHERE user = ?;");
my $updateInfo = $dbh->prepare("REPLACE INTO Info(key, data) VALUES (?, ?);");
my $getInfo = $dbh->prepare("SELECT data FROM Info WHERE key = ?;");
my $getInfoAll = $dbh->prepare("SELECT key, data FROM Info;");
my $removeInfo = $dbh->prepare("DELETE FROM Info WHERE key = ?;");
my $addTell = $dbh->prepare("INSERT INTO Tells(sender, rcpt, channel, message) VALUES (?, ?, ?, ?);");
my $getTells = $dbh->prepare("SELECT rowid, sender, channel, message FROM Tells WHERE rcpt = ?;");
my $removeTells = $dbh->prepare("DELETE FROM Tells WHERE rcpt = ?;");
my $addChannel = $dbh->prepare("REPLACE INTO Channels (name) VALUES (?);");
my $removeChannel = $dbh->prepare("DELETE FROM Channels WHERE name = ?;");
my $addOp = $dbh->prepare("REPLACE INTO Ops (name) VALUES (?);");
my $removeOp = $dbh->prepare("DELETE FROM Ops WHERE name = ?;");
# Finished setup

# Color contants
my $WHITE = "\x0300";
my $BLACK = "\x0301";
my $BLUE = "\x0302";
my $GREEN = "\x0303";
my $RED = "\x0304";
my $BROWN = "\x0305";
my $PURPLE = "\x0306";
my $ORANGE = "\x0307";
my $YELLOW = "\x0308";
my $LIGHT_GREEN = "\x0309";
my $TEAL = "\x0310";
my $LIGHT_CYAN = "\x0311";
my $LIGHT_BLUE = "\x0312";
my $PINK = "\x0313";
my $GREY = "\x0314";
my $LIGHT_GREY = "\x0315";
my $NORMAL = "\x0f";

my $lock = 1;  #Lock power commands

my @magic8ball = (
	qq<$GREEN Signs point to yes.>,
	qq<$GREEN Yes.>,
	qq<$YELLOW Reply hazy, try again.>,
	qq<$GREEN Without a doubt.>,
	qq<$RED My sources say no.>,
	qq<$GREEN As I see it, yes.>,
	qq<$GREEN You may rely on it.>,
	qq<$YELLOW Concentrate and ask again.>,
	qq<$RED Outlook not so good.>,
	qq<$GREEN It is decidedly so.>,
	qq<$YELLOW Better not tell you now.>,
	qq<$RED Very doubtful.>,
	qq<$GREEN Yes - definitely.>,
	qq<$GREEN It is certain.>,
	qq<$YELLOW Cannot predict now.>,
	qq<$GREEN Most likely.>,
	qq<$YELLOW Ask again later.>,
	qq<$RED My reply is no.>,
	qq<$GREEN Outlook good.>,
	qq<$RED Do not count on it.>,
);

my ($irc) = POE::Component::IRC->spawn();
my @channels = &LoadChannels;
my @ops = &LoadOps;

POE::Session->create(
	inline_states => {
		_start => \&bot_start,
		irc_001 => \&on_connect,
		irc_public => \&on_public,
		irc_msg => \&on_private,
		irc_join => \&on_join,
		irc_bot_mentioned => \&on_mention,
		irc_kick => \&on_kick,
		irc_disconnected => \&on_disconnect,
		got_input => \&console_io,
		parse_input => \&console_parse,
	},
);

sub bot_start {
	my $heap = $_[HEAP];
	$heap->{readline_wheel} = POE::Wheel::ReadLine->new(InputEvent => 'got_input');
	$heap->{readline_wheel}->get("> ");
	$irc->yield(register => "all");
	$irc->plugin_add( 'Logger', POE::Component::IRC::Plugin::Logger->new( Path => 'logs', DCC => 0, Private => 1, Public => 1, ) );
	$irc->plugin_add( 'Connector', POE::Component::IRC::Plugin::Connector->new());
	$irc->yield(connect => {
			Nick => $nick,
			Username => $user,
			Ircname => $name,
			Server => $server,
			Port => $port,
			Flood => 'true',
		}
	);
	$irc->plugin_add( 'NickServID', POE::Component::IRC::Plugin::NickServID->new( Password => $NSPass ) );
	$irc->plugin_add( 'BotAddressed', POE::Component::IRC::Plugin::BotAddressed->new() );
}

sub console_io {
	my ($heap, $kernel, $input, $exception) = @_[HEAP, KERNEL, ARG0, ARG1];
	if (defined $input) {
		$heap->{readline_wheel}->addhistory($input);
		$kernel->yield('parse_input' => $input);
	}
	elsif ($exception eq 'interrupt') {
		$heap->{readline_wheel}->put("Goodbye.");
		delete $heap->{readline_wheel};
		&shutdown;
		return
	} else {
		$heap->{readline_wheel}->put("\tException: $exception");
		if ($exception eq 'eot') {
			&shutdown;
			delete($heap->{readline_wheel});
		}
	}
	$heap->{readline_wheel}->get("> ") if ($heap->{readline_wheel});
	undef;
}

sub console_parse {
	my ($kernel, $heap, $input) = @_[KERNEL, HEAP, ARG0];
}

sub on_connect {
	$irc->yield(join => $_) for @channels;
	return;
}

sub on_disconnect {
	# &bot_start;	
	return;
}

sub on_public {
	&handle_public(@_[KERNEL, ARG0, ARG1, ARG2]);
}

sub handle_public {
	my ($kernel, $who, $where, $msg) = @_;
	my $nick = (split /!/, $who)[0];
	my $channel = $where->[0];
	my $dt = DateTime->now( time_zone=> 'UTC' );
	my $ts = $dt->epoch;
	my $isOp = 0;
	if ($msg =~ /youtube\.com/) {
		my @matches = ($msg =~ m/v\=.*/g);
		if (@matches > 0) {
			my $videoid = substr($matches[0],2,11);
			my $yt = new WebService::GData::YouTube();
			$yt->query->q($videoid)->limit(1,0);
			my $videos = $yt->search_video();
			if (!(defined $videos)) { return }
			my $length = $$videos[0]->{_feed}{'media$group'}{'yt$duration'}{'seconds'};
			my $time = &ConvertFromSeconds($length);
			my $url = $$videos[0]->{_feed}{link}[0]{href};
			$url =~ s/\&feature=youtube_gdata//g;
			my $uploader = $$videos[0]->{_feed}{'media$group'}{'media$credit'}[0]{'yt$display'};
			my $uploadedon = substr($$videos[0]->{_feed}{'media$group'}{'yt$uploaded'}{text}, 0, 10);
			my $views = &AddCommas($$videos[0]->{_feed}{'yt$statistics'}{viewCount});
			eval{ $irc->yield(ctcp => $channel, qq{ACTION found } . $$videos[0]->title . qq<\x02 - length: \x02> . $time . qq<\x02 - > . $views  . qq<\x02 views - uploaded by: \x02> . $uploader . qq<\x02 on \x02> . $uploadedon . qq<\x02> ); };
			$irc->yield(ctcp => $channel, qq{ACTION encountered an error while finding information}) if $@;
		}
	}
	if ($msg =~ /twitter\.com/) {
		$msg =~ /twitter\.com\/.+\/status\/(\d+)/;
		eval {
			my $status = $twitter->show_status($1);
			$irc->yield(privmsg => $channel, qq~$status->{created_at} \@$status->{user}{screen_name}: $status->{text}~);
		};
		$irc->yield(ctcp => $channel, qq{ACTION could not find that tweet on Twitter}) if $@;
	}
	foreach (@ops) {
		if ( $nick eq $_ ) { $isOp = 1; }
	}
	$getTells->execute(lc($nick));
	while (my $row = $getTells->fetchrow_hashref){
		$irc->yield(notice => $nick, $$row{'sender'} . ' in ' . $$row{'channel'} . '> ' . $$row{'message'});
		$dbh->do("DELETE FROM Tells where rowid = " . $$row{'rowid'} . ";");
	}
	$updateLastSeen->execute(lc($nick), $ts);
	if ($msg =~ /^(\(.*\) |\<.*\> |)\./) {
		$msg = substr($msg,index($msg, '.'));
		my ($command, @cmdargs) = (split / /, $msg);
		switch ($command)
		{
			case '.8ball' {
				my $response = $magic8ball[rand @magic8ball];
				my $question = "";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$question .= $cmdargs[$i] . " ";
				}
				chop($question);
				$irc->yield(privmsg => $channel, $nick . q< asked "> . $question . q<" - > . $response);
			}
			case /^\.t($|ell$)/ {
				my $numArgs = @cmdargs;
				if ($numArgs == 0) {
					$irc->yield(notice => $nick, qq<Who did you want to tell and what?>);
					return;
				}
				if ($numArgs == 1) {
					$irc->yield(notice => $nick, qq<What did you want to tell that user?>);
				} else {
					my $rcpt = $cmdargs[0];
					my $msg = "";
					for (my $i =1; $i <= $#cmdargs; $i++) {
						$msg .= $cmdargs[$i] . " ";
					}
					chop($msg);
					$addTell->execute($nick, lc($rcpt), $channel, $msg);
					$irc->yield(notice => $nick, qq<\x02$rcpt\x02 will be notified on their next activity.>);
				}
			}
			case /^\.g($|oogle$)/ {
				my $query = "";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				my $search = Google::Search->Web( query => $query );
				eval{
					my $result = $search->first;
					my $content;
					if (defined $result) {
						$content = $result->content;
					} else {
						$content = "No results found";
					}
					$content =~ s|<.+?>||g;
					$irc->yield(privmsg => $channel, $nick . q<: > . $result->unescapedUrl . qq< - \x02> . $result->titleNoFormatting . qq<\x02: "> . $content . q<">);
				};
				$irc->yield(privmsg => $channel, $nick . qq<: Error while finding information>) if $@;
			}
			case /^\.(yt$|youtube$)/ {
				my $yt = new WebService::GData::YouTube();
				my $query = "";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				eval{
					$yt->query->q($query)->limit(1,0);
					my $videos = $yt->search_video();
					my $length = $$videos[0]->{_feed}{'media$group'}{'yt$duration'}{'seconds'};
					my $time = &ConvertFromSeconds($length);
					my $url = $$videos[0]->{_feed}{link}[0]{href};
					$url =~ s/\&feature=youtube_gdata//g;
					my $uploader = $$videos[0]->{_feed}{'media$group'}{'media$credit'}[0]{'yt$display'};
					my $uploadedon = substr($$videos[0]->{_feed}{'media$group'}{'yt$uploaded'}{text}, 0, 10);
					my $views = &AddCommas($$videos[0]->{_feed}{'yt$statistics'}{viewCount});
					$irc->yield(ctcp => $channel, qq{ACTION found } . $$videos[0]->title . qq<\x02 - length: \x02> . $time . qq<\x02 - > . $url . qq< - \x02> . $views  . qq<\x02 views - uploaded by: \x02> . $uploader . qq<\x02 on \x02> . $uploadedon . qq<\x02> );
				};
				$irc->yield(ctcp => $channel, qq<ACTION encountered an error while finding information>) if $@;
			}
			case ".wiki" {
				my $query = "wiki ";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				eval{
					my $search = Google::Search->Web( query => $query );
					my $result = $search->first;
					my $content = $result->content;
					$content =~ s|<.+?>||g;
					$irc->yield(privmsg => $channel, $nick . q<: > . $result->unescapedUrl . qq< - \x02> . $result->titleNoFormatting . qq<\x02: "> . $content . q<">);
				};
				$irc->yield(privmsg => $channel, $nick . qq<: Error while finding information>) if $@;
			}			
			case ".down" {
				my $url = $cmdargs[0];
				if (!($url =~ /^http(s|):\/\//)) {
					$url = 'http://' . $url;
				}
				if ($url =~ /^https:\/\//) {
					$irc->yield(privmsg => $channel, $nick . qq<: HTTPS is not currently supported for this command.>);
				} else {
					if ($URLValidator->is_web_uri( $url )) {
						my $status = "";
						if (! head($url) ){
							print $!;
							$status = "$url appears to be " . $RED . "down" . $NORMAL . "!";
						} else {
							$status = "$url appears to be " . $GREEN . "up" . $NORMAL . "!";
						}
						$irc->yield(privmsg => $channel, $nick . qq<: $status>);
					} else {
						$irc->yield(privmsg => $channel, $nick . qq<: $url does not appear to be a valid URL.>);
					}
				}
			}
			case /^\.q($|uote$)/ {
				my $numArgs = @cmdargs;
				if ($numArgs == 0) {
					$getAnyQuote->execute;
					my $result = $getAnyQuote->fetchrow_hashref;
					$irc->yield(privmsg => $channel, '<' . $$result{'user'} . '> ' . $$result{'data'});
					return;
				}
				if ($numArgs == 1) {
					$getUserQuote->execute(lc($cmdargs[0]));
					my $result = $getUserQuote->fetch;
					if (!(defined $result)) {
						$irc->yield(privmsg => $channel, 'No quotes found for ' . $cmdargs[0] . '.');
						return;
					}
					$irc->yield(privmsg => $channel, '<' . $cmdargs[0] . '> ' . $$result[0]);
				} else {
					my $user = $cmdargs[0];
					my $quote = "";
					for (my $i =1; $i <= $#cmdargs; $i++) {
						$quote .= $cmdargs[$i] . " ";
					}
					chop($quote);
					$addQuote->execute(lc($user), $quote);
					$irc->yield(privmsg => $channel, qq<$nick: Quote added to database.>);
				}
			}
			case /^\.i($|nfo$)/ {
				my $numArgs = @cmdargs;
				if ($numArgs == 0) {
					$irc->yield(privmsg => $channel, qq<$nick: No key specified.>);
					return;
				}
				if ($numArgs == 1) {
					$getInfo->execute(lc($cmdargs[0]));
					my $result = $getInfo->fetch;
					$irc->yield(privmsg => $channel, qq<\x02> . $cmdargs[0] . qq<\x02 - > . $$result[0]);
				} else {
					my $key = lc($cmdargs[0]);
					my $data = "";
					for (my $i = 1; $i <= $#cmdargs; $i++) {
						$data .= $cmdargs[$i] . " ";
					}
					chop($data);
					$updateInfo->execute($key, $data);
					$irc->yield(privmsg => $channel, qq<$nick: Value set.>);
				}
			}
			case ".define" {
				my $query = "define:";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				eval{
					my $search = Google::Search->Web( query => $query );
					my $result = $search->first;
					#print Dumper($result);
					my $content = $result->content;
					$content =~ s|<.+?>||g;
					$irc->yield(privmsg => $channel, $nick . q<: > . $result->unescapedUrl . qq< - \x02> . $result->titleNoFormatting . qq<\x02: "> . $content . q<">);
				};
				$irc->yield(privmsg => $channel, $nick . qq<: Error while finding information>) if $@;
			}
			case ".urban" {
				my $query = "site:urbandictionary.com ";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				eval{
					my $search = Google::Search->Web( query => $query );
					my $result = $search->first;
					my $content = $result->content;
					$content =~ s|<.+?>||g;
					$irc->yield(privmsg => $channel, $nick . q<: > . $result->unescapedUrl . qq< - \x02> . $result->titleNoFormatting . qq<\x02: "> . $content . q<">);
				};
				$irc->yield(privmsg => $channel, $nick . qq<: Error while finding information>) if $@;
			}
			case /^\.(db$|devbukkit$)/ {
				my $query = "site:dev.bukkit.org ";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				my $search = Google::Search->Web( query => $query );
				eval{
					my $result = $search->first;
					my $content;
					if (defined $result) {
						$content = $result->content;
					} else {
						$content = "No results found";
					}
					$content =~ s|<.+?>||g;
					$irc->yield(privmsg => $channel, $nick . q<: > . $result->unescapedUrl . qq< - \x02> . $result->titleNoFormatting . qq<\x02: "> . $content . q<">);
				};
				$irc->yield(privmsg => $channel, $nick . qq<: Error while finding information>) if $@;
			}
			case ".seen" {
				my $numArgs = @cmdargs;
				if ($numArgs >= 1) {
					my $user = $cmdargs[0];
					$getLastSeen->execute(lc($user));
					my $result = $getLastSeen->fetch;
					if (!(defined $result)){
						$irc->yield(privmsg => $channel, qq<\x02$user\x02 does not have any known activity.>);
						return;
					}
					my $time = $$result[0];
					my $lastseen = DateTime->from_epoch ( epoch => $time );
					my $lsmsg = $lastseen->day . '-' . $lastseen->month_abbr . '-' . $lastseen->year . ' ' . $lastseen->hms . ' UTC';
					my $duration = &ConvertFromSeconds(($ts - $time));
					$irc->yield(privmsg => $channel, qq<\x02$user\x02 was last seen $lsmsg ($duration ago).>);
				}
			}
			case /^\.(brainfuck$|bf$)/ {
				$irc->yield(privmsg => $channel, qq<\x02$nick\x02 No esoteric programming languages allowed.>);
			}
			case /^\.help$/ {
				for (my $i = 0; $i <= $#help; $i++) {
					$irc->yield(notice => $nick, $help[$i][0] . ' - ' . $help[$i][1]);
				}
			}
			case ".ann" {
				my $query = "site:animenewsnetwork.com ";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				my $search = Google::Search->Web( query => $query );
				eval{
					my $result = $search->first;
					my $content;
					if (defined $result) {
						$content = $result->content;
					} else {
						$content = "No results found";
					}
					$content =~ s|<.+?>||g;
					$irc->yield(privmsg => $channel, $nick . q<: > . $result->unescapedUrl . qq< - \x02> . $result->titleNoFormatting . qq<\x02: "> . $content . q<">);
				};
				$irc->yield(privmsg => $channel, $nick . qq<: Error while finding information>) if $@;
			}
			case ".qlist" {
				my $numArgs = @cmdargs;
				if ($numArgs == 0) {
					$irc->yield(notice => $nick, qq<Who would you like to see a list quotes of?>);
					return;
				}
				if ($numArgs == 1) {
					$getUserQuoteAll->execute(lc($cmdargs[0]));
					my $result = $getUserQuoteAll->fetchall_arrayref({});
					if (@$result == 0) {
						$irc->yield(notice => $nick, 'No quotes found for ' . $cmdargs[0] . '.');
						return;						
					}
					foreach my $entry (@$result) {
						$irc->yield(privmsg => $nick, '<' . $cmdargs[0] . '> ' . $entry->{data});					
					}

				} else {
					if ($isOp != 1) {
						$irc->yield(notice => $nick, qq<Username should contain no spaces.>);
						return;
					}
					elsif ($numArgs >= 3) {
						if ($cmdargs[0] eq "remove") {
							my $quote;
							for (my $i = 2; $i <= $#cmdargs; $i++) {
								$quote .= $cmdargs[$i] . " ";
							}
							chop($quote);
							$getSpecificQuote->execute(lc($cmdargs[1]), $quote);
							my $result = $getSpecificQuote->fetchall_arrayref({});
							if (@$result != 0) {
								$removeQuote->execute(lc($cmdargs[1]), $quote);
								$irc->yield(notice => $nick, qq<The Quote: $quote>);
								$irc->yield(notice => $nick, qq<Removed $cmdargs[1]'s quote from Quote database.>);
							}
							else {
								$irc->yield(notice => $nick, qq<The quote was not in the database.  Make sure that you typed it exactly. (case sensitive).>);
							}
						}
						else {
							$irc->yield(notice => $nick, qq<.qlist Command not recognized.  Proper arguments are: remove [key]>);
						}
					}
					else {
						$irc->yield(notice => $nick, qq<.qlist command not recognized.  Too few arguments.>);
						$irc->yield(notice => $nick, qq<Proper arguments are: remove [user] [quote]>);
					}
				}
			}
			case ".ilist" {
				my $numArgs = @cmdargs;
				if ($numArgs == 0) {
					$getInfoAll->execute();
					my $result = $getInfoAll->fetchall_arrayref({});
					if (@$result == 0) {
						$irc->yield(notice => $nick, qq<There are no info entries in the database.>);
					}
					foreach my $entry (@$result)
					{
						$irc->yield(privmsg => $nick, qq<\x02> . $entry->{key} . qq<\x02 - > . $entry->{data});
					}
					return;
				}
				if ($numArgs == 1) {
					if ($cmdargs[0] eq "remove") {
						$irc->yield(notice => $nick, qq<Incorrect usage.  Proper Syntax: .ilist remove [key]>);
						return;
					}
					else {
						$irc->yield(notice => $nick, qq<.ilist Command not recognized.  Proper arguments are: remove [key]>);
					}
				}
				elsif ($numArgs == 2) {
					if ($cmdargs[0] eq "remove") {
						$getInfo->execute($cmdargs[1]);
						my $result = $getInfo->fetchall_arrayref({});
						if (@$result != 0) {
							$removeInfo->execute($cmdargs[1]);
							$irc->yield(notice => $nick, qq<Removed $cmdargs[1] from info database.>);
						}
						else {
							$irc->yield(notice => $nick, qq<The info was not in the database.>);
						}
					}
					else {
						$irc->yield(notice => $nick, qq<.ilist Command not recognized.  Proper arguments are: remove [key]>);
					}
				}
				else {
					$irc->yield(notice => $nick, qq<.ilist command not recognized.  Too many arguments.>);
					$irc->yield(notice => $nick, qq<Proper arguments are: remove [key]>);
				}
			}
			case /^\.(cf$|curseforge$)/ {
				my $query = "site:minecraft.curseforge.com ";
				for (my $i = 0; $i <= $#cmdargs; $i++) {
					$query .= $cmdargs[$i] . " ";
				}
				chop($query);
				my $search = Google::Search->Web( query => $query );
				eval{
					my $result = $search->first;
					my $content;
					if (defined $result) {
						$content = $result->content;
					} else {
						$content = "No results found";
					}
					$content =~ s|<.+?>||g;
					$irc->yield(privmsg => $channel, $nick . q<: > . $result->unescapedUrl . qq< - \x02> . $result->titleNoFormatting . qq<\x02: "> . $content . q<">);
				};
				$irc->yield(privmsg => $channel, $nick . qq<: Error while finding information>) if $@;
			}
		}
	}
	return;
}

sub on_private {
	my ($kernel, $from, $to, $msg, $identified) = @_[KERNEL, ARG0, ARG1, ARG2, ARG3];
	my $nick = (split /!/, $from)[0];
	my @fake_channel = [$nick];
	&handle_public($kernel, $from, \@fake_channel, $msg);
	my ($command, @cmdargs) = (split / /, $msg);
	my $isOp = 0;
	foreach (@ops) {
		if ( $nick eq $_ ) { $isOp = 1; }
	}
	if (substr($command,0,1) ne '.') {
		switch ($command) {
			case 'tables' {
				if ($isOp == 1) {
					foreach my $table (@tables) {
						$irc->yield(notice => $nick, $table);
					}
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'emoteas' {
				if ($isOp == 1) {
					my $response;
					for (my $i = 1; $i <= $#cmdargs; $i++) {
						$response .= $cmdargs[$i] . " ";
					}
					$irc->yield(ctcp => $cmdargs[0], qq<ACTION $response>);
					$irc->yield(notice => $nick, "Sent!");
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'speakas' {
				if ($isOp == 1) {
					my $response;
					for (my $i = 1; $i <= $#cmdargs; $i++) {
						$response .= $cmdargs[$i] . " ";
					}
					$irc->yield(privmsg => $cmdargs[0], $response);
					$irc->yield(notice => $nick, "Sent!");
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'join' {
				if ($isOp == 1) {
					$irc->yield(join => $cmdargs[0]);
					$addChannel->execute($cmdargs[0]);
					@channels = &LoadChannels;
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'part' {
				if ($isOp == 1) {
					$irc->yield(part => $cmdargs[0]);
					$removeChannel->execute($cmdargs[0]);
					@channels = &LoadChannels;
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'op' {
				if ($isOp == 1) {
					push(@ops, $cmdargs[0]);
					$addOp->execute($cmdargs[0]);
					$irc->yield(notice => $nick, "You have granted op to $cmdargs[0].");
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'deop' {
				if ($isOp == 1) {
					my $index = &Find($cmdargs[0], @ops);
					delete $ops[$index];
					$removeOp->execute($cmdargs[0]);
					$irc->yield(notice => $nick, "You have removed op from $cmdargs[0].");
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'shutdown' {
				if ($isOp == 1) {
					&shutdown;
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'lock' {
				if ($isOp == 1) {
					$lock = 1;
					$irc->yield(notice => $nick, "Restricted commands are locked.");
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'unlock' {
				if ($isOp == 1) {
					$lock = 0;
					$irc->yield(notice => $nick, "Restricted commands are unlocked.");
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'query' {
				if ($isOp == 1) {
					if ($lock == 1) {
						$irc->yield(notice => $nick, "This command is locked.");
					} else {
						if (uc($cmdargs[0]) ne 'SELECT') {
							my $rows = $dbh->do(join(" ", @cmdargs));
							$irc->yield(notice => $nick, "$rows rows affected");
						} else {
							my $results = $dbh->selectall_arrayref(join(" ", @cmdargs), { Slice => {} });
							$irc->yield(notice =>$nick, join(",", keys $$results[0]));
							foreach my $row (@$results) {
								my $string = "";
								foreach my $key (keys $row) {
									$string .= $row->{$key} . ',';
								}
								chop($string);
								$irc->yield(notice => $nick, $string);
							}
						}
					}
				} else {
					$irc->yield(notice => $nick, "You cannot do that.");
				}
			}
			case 'help' {
				for (my $i = 0; $i <= $#ophelp; $i++) {
					$irc->yield(notice => $nick, $ophelp[$i][0] . ' - ' . $ophelp[$i][1]);
				}
			}
			else {
				$irc->yield(notice => $nick, "I don't recognize that.");
			}
		}
	}
	return;
}

sub shutdown {
	$irc->yield(quit => "Nevermore!");
	$irc->yield(shutdown => "");
}

sub on_join {

	return;
}

sub on_mention {
	return; # Disable mention detection
	state $lastCall = 0;
	my ($kernel, $who, $where, $msg) = @_[KERNEL, ARG0, ARG1, ARG2];
	my $nick = (split /!/, $who)[0];
	my $channel = $where->[0];
	my $ts = DateTime->now( time_zone => 'UTC' );
	if ($ts->epoch >= ($lastCall + 60) && !($msg =~ /^[\.<]/)) {
		$irc->yield(ctcp => $channel, "ACTION senses someone talking about it.");
		$lastCall = $ts->epoch;
	}
	return;
}

sub on_kick {
	my ($kernel, $kicker, $where, $kickee, $why) = @_[KERNEL, ARG0, ARG1, ARG2, ARG3];
	my $nick = (split /!/, $kickee)[0];
	my $channel = $where;
	$irc->yield(privmsg => $channel, "Meh...  I didn't like them anyway. :P");
	return;
}

sub irc_console_connect {
	my ($peeradr, $peerport, $wheel_id) = @_[ARG0 .. ARG2];
	return;
}

sub irc_console_authed {
	my $wheel_id = $_[ARG0];
	return;
}

sub irc_console_close {
	my $wheel_id = $_[ARG0];
	return;
}

sub irc_console_rw_fail {
	my ($peeradr, $peerport) = @_[ARG0, ARG1];
	return;
}

$poe_kernel->run();
exit 0;

sub LoadOps {
	my $results = $dbh->selectall_arrayref("SELECT name FROM Ops;", { Slice => {} });
	my @ops;
	foreach my $entry ( @$results ) {
		push(@ops, $entry->{name});
	}
	if (!(@ops)) {
		print "No ops defined.  Please enter name of first op: ";
		my $firstop = <>;
		chomp($firstop);
		push(@ops, $firstop);
	}
	return @ops;
}

sub LoadChannels {
	my $results = $dbh->selectall_arrayref("SELECT name FROM Channels;", { Slice => {} });
	my @channels;
	foreach my $entry ( @$results ) {
		push (@channels, $entry->{name});
	}
	return @channels;
}

sub Find($@) {
	my $s = shift;
	$_ eq $s && return @_ while $_=pop;
	-1;
}

sub AddCommas {
	local $_ = shift;
	1 while s/^([-+]?\d+)(\d{3})/$1,$2/;
	return $_;
}

sub ConvertFromSeconds {
	my $seconds = $_[0];
	my $days = "";
	my $hours = "";
	my $minutes = "";
	if ($seconds > 86400) {
		$days = int($seconds / 86400) . "d ";
		$seconds = $seconds % 86400;
	} else {
		$days = "";
	}
	if ($seconds > 3600) {
		$hours = int($seconds / 3600) . "h ";
		$seconds = $seconds % 3600;
	} else {
		$hours = "";
	}
	if ($seconds > 60) {
		$minutes = int($seconds / 60) . "m ";
		$seconds = $seconds % 60;
	} else {
		$minutes = "";
	}
	if ($seconds > 0) {
		$seconds .= "s"
	} else {
		$seconds = "0s";
	}

	return ($days . $hours . $minutes . $seconds);
}
